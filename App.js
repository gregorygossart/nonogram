import React from 'react';

import Routes from "./src/router/Routes";

const App = () => (
  <Routes />
);

export default App;
