import React from 'react';
import { Text } from 'react-native';
import MySafeAreaView from "../../components/MySafeAreaView";

const HomeScreen = () => (
    <MySafeAreaView>
        <Text>Home Screen</Text>
    </MySafeAreaView>
);

export default HomeScreen;