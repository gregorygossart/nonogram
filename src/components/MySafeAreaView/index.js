import React from 'react';
import { Platform, SafeAreaView, StatusBar } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';

import theme from '../../../theme';

const styles = ScaledSheet.create({
  root: {
    flex: 1,
    backgroundColor: theme.colors.background,
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0
  }
});

const MySafeAreaView = ({ children }) => (
  <SafeAreaView style={styles.root}>{children}</SafeAreaView>
);

export default MySafeAreaView;
