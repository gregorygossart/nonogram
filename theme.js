const textColor = '#36414D';

const blueColor = '#4AA4F7';
const pinkColor = '#EA4375';
const yellowColor = '#FBC948';

const theme = {
  dark: false,
  spacing: 8,
  roundness: 8,
  elevation: 4,
  colors: {
    primary: blueColor,
    background: '#F7FAFC',
    surface: '#FFFFFF',
    accent: pinkColor,
    error: '#B00020',
    text: `${textColor}`,
    disabled: `${textColor}27`,
    placeholder: `${textColor}54`,
    backdrop: `${textColor}54`,
    divider: `${textColor}12`,
    blue: {
      primary: blueColor,
      secondary: `${blueColor}54`
    },
    pink: {
      primary: pinkColor,
      secondary: `${pinkColor}54`
    },
    yellow: {
      primary: yellowColor,
      secondary: `${yellowColor}54`
    }
  },
  fonts: {
    size: 16,
    regular: 'Montserrat-Regular',
    medium: 'Montserrat-Medium',
    light: 'Montserrat-Light',
    thin: 'Montserrat-Thin'
  },
  opacity: {
    primary: 0.87,
    secondary: 0.54,
    disabled: 0.27
  },
  shadow: {
    shadowColor: '#3495FF',
    shadowOffset: {
      width: 0,
      height: 8
    },
    shadowOpacity: 0.12,
    shadowRadius: 8,
    elevation: 12
  }
};

export default theme;
